// Core Global
$(document).foundation();

// Custom

// Initialize Smooth Scroll using smooth-scroll.js (https://github.com/cferdinandi/smooth-scroll)
smoothScroll.init({
  speed: 500, // Integer. How fast to complete the scroll in milliseconds
  easing: 'easeInOutQuad', // Easing pattern to use
  offset: 60, // Integer. How far to offset the scrolling anchor location in pixels
  updateURL: true, // Boolean. If true, update the URL hash on scroll
});

// Initialize Datepicker using foundation-datepicker.js (http://foundation-datepicker.peterbeno.com/example.html)
$(function() {
  $('.datetimepicker').fdatepicker({
    initialDate: new Date(),
    format: 'mm-dd-yyyy hh:ii',
    disableDblClickSelection: true,
    language: 'en',
    pickTime: true
  });
});

// Replace main header with fixed nav on scroll
$(window).scroll(function() {
  if ($(this).scrollTop() > 300) {
    // replace main header with fixed nav
    $('.logged-out').removeClass('main-nav-top').addClass('main-nav');
  } else {
    $('.logged-out').removeClass('main-nav').addClass('main-nav-top');
  }
});

// Close off canvas when menu item clicked
$('.off-canvas a').on('click', function() {
  $('.off-canvas').foundation('close');
});

// Select friend toggle
$('.friend-select').click(function() {
  $(this).closest('article').find('.friend-select').toggleClass('friend-select-active');
});

// Select message toggle
$('.message-select').click(function() {
  $('.message-select').removeClass("message-select-active");
  $(this).toggleClass("message-select-active");});

// Select Ya Na
$('a.adventure-type-select').click(function() {
  $('a.adventure-type-select').removeClass("select-active");
  $(this).addClass("select-active");
});

// Adventure type select active
$('.ya-na-btn').click(function() {
  $('.ya-na-btn').removeClass("ya-na-btn-active");
  $(this).addClass("ya-na-btn-active");
});

// Club role select active
$('.club-role-btn').click(function() {
  $('.club-role-btn').removeClass("club-role-active");
  $(this).toggleClass("club-role-active");
});

// Public select tab
function open_tab(tab_id, panel) {
  $("#" + tab_id).foundation("selectTab", $("#" + panel));
};

// Toggle map show / hide text
$(".toggle-map-btn").click(function() {
  if ($(this).text() == "Show Map")
    $(this).text("Hide Map")
  else
    $(this).text("Show Map");
});
